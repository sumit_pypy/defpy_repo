from django import template

register = template.Library()

@register.filter
def get_split(value):
    return value.split(",")