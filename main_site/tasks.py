from __future__ import absolute_import

from celery import Celery
import os
from celery import app 

celery = Celery('tasks', broker = 'amqp://guest@localhost//') #!

import os
from celery.decorators import task

#os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'defpy.settings')

#app = Celery('main_site', broker = 'amqp://guest@localhost//')

@task
def add(x, y):
    return x + y


# @shared_task
# def mul(x, y):
#     return x*y
# @shared_task
# def xsum(numbers):
#     return sum(numbers)

@task
def send_async_email(subject, email, from_email, to_email):
    try:
        from main_site.utils import send_html_email 
        send_html_email(subject, email, from_email, [to_email], False, None, None, None, email)
    except Exception as e:
        print e
