from django.db import models
from ckeditor.fields import RichTextField
from django.utils.translation import ugettext, ugettext_lazy as _
from django.contrib.auth.models import User
# Create your models here.
class Test(models.Model):
	text = RichTextField()

class Technology(models.Model):
	tech_name = models.CharField(_('Technology Name'),max_length=100, blank = False, null = False)
	why_learn = RichTextField(_('Why Learn'))
	in_news = RichTextField(_('In News'))
	def __unicode__(self):
		return self.tech_name

class Course(models.Model):
	tech_name = models.ForeignKey(Technology)
	name = models.CharField(_('Name'),max_length=100, blank = False, null = False)
	logo = models.ImageField(_('Logo'),upload_to="uploads/course_logo/")
	meta = models.CharField(_('Meta'),max_length=100, blank = True, null = True)
	is_workshop = models.BooleanField(default = False)
	is_active = models.BooleanField(default = True)
	keywords = models.CharField(max_length = 500, blank = True, null = True)
	def __unicode__(self):
		return self.name

class TrainingBatch(models.Model):
	course = models.ForeignKey(Course,null=False,blank=False)
	starts = models.DateField(_('Starts'),null=True,blank=True)
	duration = models.CharField(_('Duration'),max_length = 100)
	start_time = models.TimeField(_('Start Time'),null=True,blank=True)
	end_time = models.TimeField(_('End Time'),null=True,blank=True)
	fee = models.IntegerField(_('Fee (INR)'), max_length = 10)
	training_type = models.CharField(_('Training Type'), max_length = 50)
	is_active = models.BooleanField(default = True)

	def __unicode__(self):
		return self.course.name +" "+self.duration

class WorkshopBatch(models.Model):
	course = models.ForeignKey(Course,null=False,blank=False)
	duration = models.CharField(_('Duration'),max_length = 100)
	is_active = models.BooleanField(default = True)

	def __unicode__(self):
		return self.course.name +" "+self.duration


class Review(models.Model):
	tech_name = models.ForeignKey(Technology,null=False,blank=False)
	person_name = models.CharField(_('Name'),max_length=100, blank = False, null = False)
	person_email = models.EmailField(_('Email'))
	person_phone = models.BigIntegerField(_('Phone'),max_length=20, blank = True, null = True)
	photo = models.ImageField(_('Photo'),upload_to="uploads/person_photo/")
	organization = models.CharField(_('Organization'),max_length=100, blank = True, null = True)
	position = models.CharField(_('Position'),max_length=100, blank = True, null = True)
	rating = models.IntegerField(_('Rating (max-5)'), max_length = 1)
	social_platform  = models.CharField(_('Social Platform'),max_length=20, blank = False, null = False)
	social_link = models.CharField(_('Social Link'),max_length=50, blank = False, null = False)
	review = RichTextField(_('Review'))
	is_display = models.BooleanField(default = True)

	def __unicode__(self):
		return self.person_name

class Promotion(models.Model):
	contact_name=models.CharField(_('Name'),max_length=100, blank = True, null = True)
	contact_email=models.EmailField(_('Email'),max_length=100, blank = False, null = False)
	contact_phone=models.BigIntegerField(_('Phone'),max_length=20, blank = True, null = True)

	def __unicode__(self):
		return self.contact_email

class UploadContacts(models.Model):
	filename = models.FileField(upload_to="uploads/contacts/")
	uploaddate=models.DateTimeField(auto_now=True)
	# def __unicode__(self):
	#   pass

class Enquiry(models.Model):
	full_name = models.CharField(_('Full Name'),max_length=100, blank = True, null = True)
	phone = models.BigIntegerField(_('Phone'),max_length=20, blank = True, null = True)
	email = models.EmailField(_('Email'),max_length=100, blank = False, null = False)
	query = models.TextField(_('Query'))
	timestamp = models.DateTimeField(auto_now=True)
	is_active = models.BooleanField(default=True)


	def __unicode__(self):
		return self.email

class GreatSaying(models.Model):
	tech_name = models.ForeignKey(Technology,null=False,blank=False)
	saying = RichTextField(_('Saying'))
	by_whom = models.CharField(_('By Whom'),max_length=100, blank = False, null = False)
	organization = models.CharField(_('Organization'),max_length=100, blank = False, null = False)
	designation = models.CharField(_('Designation'),max_length=100, blank = False, null = False)
	
	def __unicode__(self):
		return self.by_whom

class Payment(models.Model):
	paid_timestamp = models.DateTimeField(null=True,blank=True)
	latest_status = models.CharField(_('Status'),max_length=10, blank = False, null = False, default = "UNPAID")
	def __unicode__(self):
		return self.latest_status

class Enroll(models.Model):
	enroll_id = models.CharField(_('Enroll ID'),max_length=10, blank = False, null = False)
	user = models.ForeignKey(User,null=False,blank=False)
	mobile = models.BigIntegerField(_('Phone'),max_length=20, blank = False, null = False)
	highest_qualification = models.CharField(_('Highest Qualification'),max_length=100, blank = False, null = False)
	country = models.CharField(_('Country'),max_length=20, blank = False, null = False)
	state = models.CharField(_('State'),max_length=20, blank = True, null = True)
	city = models.CharField(_('City'),max_length=20, blank = True, null = True)
	organization = models.CharField(_('Organization'),max_length=200, blank = True, null = True)
	batch = models.ForeignKey(TrainingBatch,null=False,blank=False)
	enroll_timestamp = models.DateTimeField(auto_now=True)
	payment = models.ForeignKey(Payment, null = False, blank = False)
	def __unicode__(self):
		return self.enroll_id



class AdvisoryBoard(models.Model):
	full_name = models.CharField(_('Full Name'),max_length=100, blank = False, null = False)
	email = models.EmailField(_('Email'))
	phone = models.BigIntegerField(_('Phone'),max_length=20, blank = False, null = False)
	expertise = models.CharField(_('Expertise'),max_length=100, blank = False, null = False)
	photo = models.ImageField(_('Photo'),upload_to="uploads/advisory_board/")
	profile = RichTextField(_('Profile'))
	is_active = models.BooleanField(default = True)
	def __unicode__(self):
		return self.email


class CourseContent(models.Model):
	course = models.ForeignKey(Course,max_length=100, blank = False, null=False)
	main_heading = models.CharField(_('Main Heading'),max_length=100,null=False,blank=False)
	sub_heading = models.CharField(_('Sub Heading'),max_length=1000,null=False,blank=False)
	def __unicode__(self):
		return self.course.name + " "+self.main_heading


class StaticInfo(models.Model):
	fa_icon = models.CharField(max_length=20, blank=False)
	info = models.CharField(max_length=50, blank=False,null=False)
	is_active = models.BooleanField(default=True)
	def __unicode__(self):
		return self.info

class SocialLinks(models.Model):
	link_icon = models.CharField(max_length=20, blank=False, null=False)
	link_dest = models.CharField(max_length=50, blank=False, null=False)
	is_active = models.BooleanField(default=True)
	def __unicode__(self):
		return self.link_dest

class Feature(models.Model):
	sentence = models.CharField(max_length=200, null=False, blank=False)
	is_active = models.BooleanField(default=True)
	def __unicode__(self):
		return self.sentence 
		
			

