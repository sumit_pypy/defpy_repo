# Create your views here.
from django.conf import settings
from django.core.mail import get_connection
from django.template import loader
from django.core.mail import send_mail
from django.core.mail.message import (
    EmailMessage, EmailMultiAlternatives,
    SafeMIMEText, SafeMIMEMultipart,
    DEFAULT_ATTACHMENT_MIME_TYPE, make_msgid,
    BadHeaderError, forbid_multi_line_headers)

# TODO add html email template
def send_welcome_email(email, name):
    """Send a store new account welcome mail to `email`."""
    subject = "Welcome to the fabulous world of technosia"
    from_email = settings.EMAIL_HOST_USER
    email_message = loader.render_to_string('email/testmail.html', locals())
    send_html_email(subject, email_message, from_email,[email], False, None, None, None, email_message)


def send_html_email(subject, message, from_email, recipient_list,
              fail_silently=False, auth_user=None, auth_password=None,
              connection=None, htmlmessage=None):
    
    connection = connection or get_connection(username=settings.EMAIL_HOST_USER,
                                    password=settings.EMAIL_HOST_PASSWORD,
                                    fail_silently=fail_silently)
    headers = {'Reply-To': 'no-reply@technosia.in'}
    mail = EmailMultiAlternatives(subject, message, from_email, recipient_list,
                        connection=connection,headers=headers)
    if htmlmessage:
        mail.attach_alternative(htmlmessage, 'text/html')
    mail.content_subtype = "html"
    return mail.send()