from main_site.models import *

def get_active_courses(request):
	context = {}
	# all active python courses
	pycourses = Course.objects.filter(is_active = True, tech_name__tech_name = 'Python')
	context['pycourses'] = pycourses

	# all active linux courses
	linuxcourses = Course.objects.filter(is_active = True, tech_name__tech_name = 'Linux')
	context['linuxcourses'] = linuxcourses

	# all active bigdata courses
	bigcourses = Course.objects.filter(is_active = True, tech_name__tech_name = 'Big Data & Hadoop')
	context['bigcourses'] = bigcourses

	# all active cloud courses
	cloudcourses = Course.objects.filter(is_active = True, tech_name__tech_name = 'Cloud Computing')
	context['cloudcourses'] = cloudcourses

	return context