# Create your views here.
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.template import RequestContext
from main_site.models import *
from main_site.tasks import *
from utils import *
from django.conf import settings
from django.contrib.auth.models import User
def home(request):
	ctx = RequestContext(request)
	temp_name = "main_site/index.html"
	courses = Course.objects.filter(is_active = True)
	static_infos = StaticInfo.objects.filter(is_active = True)
	social_links=SocialLinks.objects.filter(is_active = True)
	# subject = "Test subject"
	# email = "test email"
	# from_email = settings.EMAIL_HOST_USER
	# # for i in range(0,100):
	# to_email = 'vikas.sparklab@gmail.com'
	#send_async_email.apply_async([subject, email, from_email, to_email])
	#send_async_email.apply_async(('vikas', 'vikas.sparklab@gmail.com'))
	features = Feature.objects.filter(is_active = True)
	return render_to_response(temp_name, locals(), ctx)


def detail(request,course_id):
	ctx = RequestContext(request)
	temp_name = "main_site/detail.html"
	course = Course.objects.get(id = course_id,is_active = True)
	training_batch = TrainingBatch.objects.filter(course__id = course.id)
	reviews = Review.objects.filter(tech_name__id = course.tech_name.id)
	great_sayings = GreatSaying.objects.filter(tech_name__id = course.tech_name.id)
	coursecontent=CourseContent.objects.filter(course__id = course.id)
	return render_to_response(temp_name, locals(), ctx)

def about(request):
	ctx = RequestContext(request)
	temp_name = "main_site/about_us.html"
	courses = Course.objects.filter(is_active = True, id__in = [2,1,8,10,11])
	reviews = Review.objects.filter(is_display = True)
	return render_to_response(temp_name, locals(), ctx)

def submit_query(request):
	try:
		if request.method == 'POST':
			full_name = request.POST.get('name')
			email = request.POST.get('email')
			mobile = request.POST.get('mobile')
			query = request.POST.get('query')
			enq = Enquiry()
			enq.full_name = full_name
			enq.email = email
			enq.phone = mobile
			enq.query = query
			enq.save()
			return HttpResponse("Your Query Submitted, Soon we will contact back you.")
		else:
			return render_to_response("main_site/index.html", {},ctx)
	except Exception as e:
		return HttpResponse(e)


def advisory_board(request):
	ctx = RequestContext(request)
	temp_name = "main_site/advisory_board.html"
	members = AdvisoryBoard.objects.filter(is_active = True)
	return render_to_response(temp_name, locals(), ctx)

def enroll(request):
	ctx = RequestContext(request)
	if request.method == 'POST':
		full_name = request.POST.get('full_name')
		email = request.POST.get('email')
		mobile = request.POST.get('mobile')
		highest_qualification = request.POST.get('highest_qualification')
		location = request.POST.get('location')
		organization = request.POST.get('organization')
		batch_id = request.POST.get('batch_id')
		try:
			# creating user
			user = User()
			user.first_name = full_name
			user.email = email
			user.username = email.split("@")[0]
			user.is_active = True
			user.is_staff = False
			user.save()

			# creating enrollment
			enroll = Enroll()
			enroll.user = user
			enroll.enroll_id = 454545
			enroll.student_email = user
			enroll.mobile = mobile
			enroll.highest_qualification = highest_qualification
			if len(location.split(",")) == 3:
				enroll.country = location.split(",")[0]
				enroll.state = location.split(",")[1]
				enroll.city = location.split(",")[2]
			elif len(location.split(",")) == 2:
				enroll.country = location.split(",")[0]
				enroll.state = location.split(",")[1]
			else:
				enroll.country = location.split(",")[0]

			enroll.organization = organization

			# lookup for training batch
			batch = TrainingBatch.objects.get(id = batch_id)
			enroll.batch = batch
			
			#payment
			payment = Payment()
			payment.latest_status = "UNPAID"
			payment.save()
			enroll.payment = payment
			enroll.save()
			temp_name = "main_site/order_summery.html"
			return render_to_response(temp_name, locals(), ctx)
		except Exception as e:
			print e
			return HttpResponse("Some error occured please try later...")

	else:
		pass

def contact(request):
	ctx = RequestContext(request)
	temp_name = "main_site/contact.html"
	return render_to_response(temp_name, locals(), ctx)


def console(request):
	ctx = RequestContext(request)
	temp_name = "main_site/console.html"
	return render_to_response(temp_name, locals(), ctx)

def test(request):
	ctx = RequestContext(request)
	temp_name = "main_site/test.html"
	#members = AdvisoryBoard.objects.filter(is_active = True)
	return render_to_response(temp_name, locals(), ctx)

def testemail(request):
	ctx = RequestContext(request)
	temp_name = "email/python_training_program.html"
	#members = AdvisoryBoard.objects.filter(is_active = True)
	return render_to_response(temp_name, locals(), ctx)


