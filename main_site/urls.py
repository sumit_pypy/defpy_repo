from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'main_site.views.home', name='home'),
    url(r'^detail/(?P<course_id>\d+)$', 'main_site.views.detail', name = 'detail'),
    url(r'^about/$', 'main_site.views.about', name = 'about'),
    url(r'^submit-query/$', 'main_site.views.submit_query', name = 'submit_query'),
    url(r'^advisory-board/$', 'main_site.views.advisory_board', name = 'advisory_board'),
    url(r'^enroll/$', 'main_site.views.enroll', name = 'enroll'),
    url(r'^contact/$', 'main_site.views.contact', name = 'contact'),
    url(r'^console/$', 'main_site.views.console', name = 'console'),
    url(r'^test/$', 'main_site.views.test', name = 'test'),
    url(r'^testemail/$', 'main_site.views.testemail', name = 'testemail'),
    #url(r'^', include('main_site.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)
