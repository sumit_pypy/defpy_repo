from django.core.management.base import NoArgsCommand

from django.conf import settings
from django.template import loader
from main_site.tasks import *

class Command(NoArgsCommand):
    help = "send promotional emails"

    def handle_noargs(self, **options):
        print "Started ........."
        subject = "Test subject"
        email = "test email"
        from_email = settings.EMAIL_HOST_USER
        from main_site.models import Promotion
        promo = Promotion.objects.all()
        print promo
        for p in promo:
            to_email = p.contact_email
            #print to_email
            email = loader.render_to_string('email/testemail.html', locals())
            try:
                send_async_email.apply_async([subject, email, from_email, to_email])
            except Exception as e:
                print e, to_email
        
        print "Done..........."
