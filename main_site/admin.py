from django.contrib import admin
from main_site.models import *
import xlrd
# class TestAdmin(admin.ModelAdmin):
# 	list_display = ['text']

class TechnologyAdmin(admin.ModelAdmin):
	list_display = ['tech_name']


class CourseContentInline(admin.TabularInline):
    model = CourseContent

class CourseAdmin(admin.ModelAdmin):
	list_display = ['name', 'show_logo', 'meta', 'is_active']
	search_fields = ['name']
	
	def show_logo(self, obj = None):
		return "<img src = '"+obj.logo.url +"' alt = 'test image' width = 200 height = 100>"
	show_logo.allow_tags = True

	inlines = [CourseContentInline]

class TrainingBatchAdmin(admin.ModelAdmin):
	list_display = ['course',  'starts', 'duration', 'start_time', 'end_time', 'fee', 'training_type']
	list_filter = ['training_type']
	def course(self, obj = None):
		return self.course.name

class ReviewAdmin(admin.ModelAdmin):
	list_display = ['person_name', 'person_email', 'person_phone', 'rating']

class EnquiryAdmin(admin.ModelAdmin):
	list_display = ['full_name', 'email', 'phone', 'timestamp','query']
	search_fields = ['full_name', 'email', 'phone']

class PromotionAdmin(admin.ModelAdmin):
	list_display = ['contact_name', 'contact_email', 'contact_phone']
	search_fields = ['contact_name', 'contact_email', 'contact_phone']

class UploadContactsAdmin(admin.ModelAdmin):
	list_display = ['filename', 'uploaddate']

	def save_model(self, request, obj, form, change):
		try:
			sh=xlrd.open_workbook(file_contents=request.FILES['filename'].read())
			sheet = sh.sheet_by_index(0)
			for r in range(1, sheet.nrows):
				try:
					contact_name = str(sheet.row_values(r)[0]).strip()
					contact_email = str(sheet.row_values(r)[1]).strip()
					contact_phone = str(sheet.row_values(r)[2]).strip()
					print contact_name, contact_email, contact_phone
					promotion = Promotion()
					if contact_name:
						promotion.contact_name = contact_name
					else:
						promotion.contact_name = "DefPy User"
					promotion.contact_email = contact_email
					if contact_phone:
						promotion.contact_phone = contact_phone.split(".")[0]
					else:
						promotion.contact_phone = None
					promotion.save()
				except Exception as e:
					print e
						
			obj.save()
		except Exception as e:
			print e
		print "here"
		
class GreatSayingAdmin(admin.ModelAdmin):
	list_display = ['by_whom','organization']

class EnrollAdmin(admin.ModelAdmin):
	list_display = ['enroll_id','user','mobile', 'highest_qualification', 'country', 'state', 'city', 'organization', 'enroll_timestamp']

	def user(self, obj = None):
		return self.user.email

class PaymentAdmin(admin.ModelAdmin):
	list_display = ['latest_status','paid_timestamp']

class AdvisoryBoardAdmin(admin.ModelAdmin):
	list_display = ['full_name','email', 'phone', 'expertise']




# class QuestionAdmin(admin.ModelAdmin):
#     fieldsets = [
#         (None,               {'fields': ['question_text']}),
#         ('Date information', {'fields': ['pub_date'], 'classes': ['collapse']}),
#     ]
#     inlines = [ChoiceInline]

# admin.site.register(Question, QuestionAdmin)

# admin.site.register(Test, TestAdmin)
admin.site.register(Promotion, PromotionAdmin)
admin.site.register(UploadContacts, UploadContactsAdmin)
admin.site.register(Technology, TechnologyAdmin)
admin.site.register(Course, CourseAdmin)
admin.site.register(TrainingBatch, TrainingBatchAdmin)
admin.site.register(Review, ReviewAdmin)
admin.site.register(Enquiry, EnquiryAdmin)
admin.site.register(GreatSaying, GreatSayingAdmin)
admin.site.register(Enroll, EnrollAdmin)
admin.site.register(Payment, PaymentAdmin)
admin.site.register(AdvisoryBoard, AdvisoryBoardAdmin)
admin.site.register(StaticInfo)
admin.site.register(SocialLinks)
admin.site.register(Feature)
